import { BackgroundElement } from './BackgroundElement';

export class FishLarvaEgg extends BackgroundElement {
    private x: number;
    private y: number;
    private a: number;
    private readonly s: number;
    private readonly radius: number;
    private readonly color: string;

    constructor(canvas: CanvasRenderingContext2D) {
        super(canvas);
        const random = Math.random();

        this.x = this.width / 2 + (Math.random() * 200 - Math.random() * 200);
        this.y = this.height / 2 + (Math.random() * 200 - Math.random() * 200);

        this.s = Math.random();
        this.a = 0;

        this.radius = random * 0.8;
        this.color = random > 0.8 ? '#82a0c4' : '#2E4765';
    }

    public render() {
        this.canvas.beginPath();
        this.canvas.arc(this.x, this.y, this.radius, 0, 2 * Math.PI);
        this.canvas.lineWidth = 2;
        this.canvas.fillStyle = this.color;
        this.canvas.fill();
        this.canvas.closePath();
    }

    public move() {
        this.x += Math.cos(this.a) * this.s;
        this.y += Math.sin(this.a) * this.s;
        this.a += Math.random() * 0.8 - 0.4;

        if (this.x < 0 || this.x > this.width - this.radius) {
            return false;
        }

        if (this.y < 0 || this.y > this.height - this.radius) {
            return false;
        }
        this.render();
        return true;
    }
}
