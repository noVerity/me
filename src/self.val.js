// This file takes the source code of the whole page and returns the contents + the filename
// It is loaded via the val-loader in webpack so it is executed in the node context on build

const fs = require('fs');
const path = require('path');

let requests = [],
  _ = require('lodash');

const walkSync = (dir, filelist = []) => fs.readdirSync(dir)
  .map(file => fs.statSync(path.join(dir, file)).isDirectory()
    ? walkSync(path.join(dir, file), filelist)
    : filelist.concat(path.join(dir, file))[0]);

const matchFile = (file) => {
  if (file instanceof Array) {
    file.forEach(matchFile);
    return;
  }
  if (file.match(/\.js$/) !== null) {
    let promise = new Promise((resolve) => {
      fs.readFile(file, { encoding: 'utf8'}, (err, data) => {
        let content = ''
        if (!err) content = data;
        resolve({
          name: path.relative(__dirname, file),
          content: content
        })
      });
    })
    requests.push(promise);
  }
};


module.exports = function() {
  requests = [];
  let dependencies = _.flattenDeep(walkSync(__dirname));

  dependencies.forEach(matchFile);

  return Promise.all(requests)
    .then((results) => {
      return {
        code: 'module.exports = ' + JSON.stringify(results),
        dependencies: dependencies,
        cacheable: true
      }
    });
};