import { render } from '@testing-library/react';
import React from 'react';
import App from './App';

test('renders me', () => {
    const { getByText } = render(<App />);
    const linkElement = getByText(/Florian Adomeit/i);
    expect(linkElement).toBeInTheDocument();
});
